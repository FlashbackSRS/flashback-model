package fb

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Package represents a top-level collection of Flashback Documents, such that
// they can be easily transmitted or shared as a single file. It is intended to
// be used via its json.Marshaler and json.Unmarshaler interfaces.
type Package struct {
	Created  time.Time `json:"created"`
	Modified time.Time `json:"modified"`
	Bundle   *Bundle   `json:"bundle,omitempty"`
	Cards    []*Card   `json:"cards,omitempty"`
	Notes    []*Note   `json:"notes,omitempty"`
	Decks    []*Deck   `json:"decks,omitempty"`
	Themes   []*Theme  `json:"themes,omitempty"`
	Reviews  []*Review `json:"reviews,omitempty"`
}

type packageAlias Package

// MarshalJSON implements the json.Marshaler interface for the Package type.
func (p *Package) MarshalJSON() ([]byte, error) {
	type encPackage struct {
		packageAlias
		Created  shortTime `json:"created"`  // nolint: govet
		Modified shortTime `json:"modified"` // nolint: govet
	}

	if err := p.Validate(); err != nil {
		return nil, err
	}
	doc := encPackage{
		packageAlias: packageAlias(*p),
		Created:      shortTime{p.Created},
		Modified:     shortTime{p.Modified},
	}
	return json.Marshal(doc)
}

// Validate does some basic sanity checking on the package.
func (p *Package) Validate() error {
	if p == nil {
		return errors.New("package is nil")
	}
	if p.Bundle == nil {
		return errors.New("bundle is nil")
	}
	bundleID := strings.TrimPrefix(p.Bundle.ID, "bundle-")
	cardMap := map[string]*Card{}
	for _, c := range p.Cards {
		if bid := strings.Split(strings.TrimPrefix(c.ID, "card-"), ".")[0]; bid != bundleID {
			return fmt.Errorf("card '%s' not in bundle '%s'", c.ID, p.Bundle.ID)
		}
		cardMap[c.ID] = c
	}

	for _, d := range p.Decks {
		for _, id := range d.Cards.All() {
			if _, ok := cardMap[id]; !ok {
				return fmt.Errorf("card '%s' listed in deck, but not found in package", id)
			}
			delete(cardMap, id)
		}
	}
	for id := range cardMap {
		return fmt.Errorf("card '%s' found in package, but not in a deck", id)
	}

	modelMap := make(map[string]*Model)
	for _, t := range p.Themes {
		for _, m := range t.Models {
			modelMap[fmt.Sprintf("%s/%d", t.ID, m.ID)] = m
		}
	}
	for _, n := range p.Notes {
		key := fmt.Sprintf("%s/%d", n.ThemeID, n.ModelID)
		m, ok := modelMap[key]
		if !ok {
			return errors.Errorf("note '%s' has no matching model (%s)", n.ID, key)
		}
		n.Model = m
	}

	return nil
}

// SetOwner sets the owner of the package, recursively updating the bundle,
// cards, and decks.
func (p *Package) SetOwner(owner string) error {
	if p == nil {
		return errors.New("package is nil")
	}
	if p.Bundle == nil {
		return errors.New("bundle is nil")
	}
	parts := strings.Split(p.Bundle.ID, "-")
	bid := parts[len(parts)-1]
	p.Bundle.ID = "bundle-" + owner + "-" + bid
	for _, card := range p.Cards {
		card.ID = updateCardID(card.ID, bid, owner)
	}
	for _, deck := range p.Decks {
		for card := range deck.Cards {
			delete(deck.Cards, card)
			deck.Cards[updateCardID(card, bid, owner)] = struct{}{}
		}
	}
	return nil
}

func updateCardID(cardID, bundleID, owner string) string {
	// Keep the interesting bits
	id := strings.Join(strings.Split(cardID, ".")[1:], ".")
	return "card-" + owner + "-" + bundleID + "." + id
}
