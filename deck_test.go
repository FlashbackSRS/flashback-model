package fb

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestCCMarshalJSON(t *testing.T) {
	type Test struct {
		name     string
		cc       *CardCollection
		expected string
		err      string
	}
	tests := []Test{
		{
			name:     "empty",
			cc:       &CardCollection{},
			expected: "[]",
		},
		{
			name: "some cards",
			cc: &CardCollection{
				"card-mjxwe-Zm9v.bmlsCg.0": {},
				"card-mjxwe-YmFy.bmlsCg.0": {},
			},
			expected: `["card-mjxwe-YmFy.bmlsCg.0","card-mjxwe-Zm9v.bmlsCg.0"]`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := test.cc.MarshalJSON()
			testy.Error(t, test.err, err)
			if d := testy.DiffJSON([]byte(test.expected), result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestCCUnmarshalJSON(t *testing.T) {
	type Test struct {
		name     string
		input    string
		expected *CardCollection
		err      string
	}
	tests := []Test{
		{
			name:  "invalid json",
			input: "invalid json",
			err:   "invalid character 'i' looking for beginning of value",
		},
		{
			name:  "valid",
			input: `["card-mjxwe-Zm9v.bmlsCg.0","card-mjxwe-YmFy.bmlsCg.0"]`,
			expected: &CardCollection{
				"card-mjxwe-Zm9v.bmlsCg.0": {},
				"card-mjxwe-YmFy.bmlsCg.0": {},
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := &CardCollection{}
			err := result.UnmarshalJSON([]byte(test.input))
			testy.Error(t, test.err, err)
			if d := testy.DiffInterface(test.expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestCCAll(t *testing.T) {
	cc := &CardCollection{
		"card-mjxwe-Zm9v.bmlsCg.0": {},
		"card-mjxwe-YmFy.bmlsCg.0": {},
	}
	expected := []string{"card-mjxwe-YmFy.bmlsCg.0", "card-mjxwe-Zm9v.bmlsCg.0"}
	result := cc.All()
	if d := testy.DiffInterface(expected, result); d != nil {
		t.Error(d)
	}
}

func TestNewDeck(t *testing.T) {
	type Test struct {
		name     string
		id       string
		expected *Deck
	}
	tests := []Test{
		{
			name: "valid",
			id:   "deck-Zm9vIGlkCg",
			expected: &Deck{
				ID:       "deck-Zm9vIGlkCg",
				Created:  shortNow,
				Modified: shortNow,
				Cards:    CardCollection{},
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := NewDeck(test.id)
			if d := testy.DiffInterface(test.expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestDeckMarshalJSON(t *testing.T) {
	type Test struct {
		name     string
		deck     *Deck
		expected string
		err      string
	}
	tests := []Test{
		{
			name: "full fields",
			deck: &Deck{
				ID:          "deck-ZGVjaw",
				Created:     now(),
				Modified:    now(),
				Imported:    now(),
				Name:        "test name",
				Description: "test description",
				Cards: CardCollection{
					"card-mjxwe-Zm9v.bmlsCg.0": {}, "card-mjxwe-YmFy.bmlsCg.0": {},
				},
			},
			expected: `{
				"_id":         "deck-ZGVjaw",
				"type":        "deck",
				"name":        "test name",
				"description": "test description",
				"created":     "2017-01-01T00:00:00Z",
				"modified":    "2017-01-01T00:00:00Z",
				"imported":    "2017-01-01T00:00:00Z",
				"cards":       ["card-mjxwe-YmFy.bmlsCg.0","card-mjxwe-Zm9v.bmlsCg.0"]
			}`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := test.deck.MarshalJSON()
			testy.Error(t, test.err, err)
			if d := testy.DiffJSON([]byte(test.expected), result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestDeckAddCard(t *testing.T) {
	deck := NewDeck("deck-Zm9v")
	deck.AddCard("card-jack")
	deck.AddCard("card-jill")
	expected := &Deck{
		ID:       "deck-Zm9v",
		Created:  shortNow,
		Modified: shortNow,
		Cards: CardCollection{
			"card-jack": {},
			"card-jill": {},
		},
	}
	if d := testy.DiffInterface(expected, deck); d != nil {
		t.Error(d)
	}
}

func TestDeckUnmarshalJSON(t *testing.T) {
	type Test struct {
		name     string
		input    string
		expected *Deck
		err      string
	}
	tests := []Test{
		{
			name:  "invalid json",
			input: "invalid json",
			err:   "invalid character 'i' looking for beginning of value",
		},
		{
			name: "all fields",
			input: `{
                "_id":         "deck-ZGVjaw",
                "name":        "test name",
                "description": "test description",
                "created":     "2017-01-01T00:00:00Z",
                "modified":    "2017-01-01T00:00:00Z",
                "imported":    "2017-01-01T00:00:00Z",
                "cards":       ["card-mjxwe-YmFy.bmlsCg.0","card-mjxwe-Zm9v.bmlsCg.0"]
            }`,
			expected: &Deck{
				ID:          "deck-ZGVjaw",
				Created:     shortNow,
				Modified:    shortNow,
				Imported:    shortNow,
				Name:        "test name",
				Description: "test description",
				Cards: CardCollection{
					"card-mjxwe-YmFy.bmlsCg.0": {},
					"card-mjxwe-Zm9v.bmlsCg.0": {},
				},
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := &Deck{}
			err := result.UnmarshalJSON([]byte(test.input))
			testy.Error(t, test.err, err)
			if d := testy.DiffInterface(test.expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestDeckSetRev(t *testing.T) {
	deck := &Deck{}
	rev := rev1
	deck.SetRev(rev)
	if deck.Rev != rev {
		t.Errorf("Unexpected result: %s", deck.Rev)
	}
}

func TestDeckDocID(t *testing.T) {
	deck := &Deck{ID: "deck-Zm9v"}
	expected := "deck-Zm9v"
	if id := deck.DocID(); id != expected {
		t.Errorf("Unexpected result: %s", id)
	}
}

func TestDeckImportedTime(t *testing.T) {
	t.Run("Set", func(t *testing.T) {
		ts := now()
		deck := &Deck{Imported: ts}
		if it := deck.ImportedTime(); it != ts {
			t.Errorf("Unexpected result: %s", it)
		}
	})
	t.Run("Unset", func(t *testing.T) {
		deck := &Deck{}
		if it := deck.ImportedTime(); !it.IsZero() {
			t.Errorf("unexpected result: %v", it)
		}
	})
}

func TestDeckModifiedTime(t *testing.T) {
	deck := &Deck{}
	ts := now()
	deck.Modified = ts
	if mt := deck.ModifiedTime(); mt != ts {
		t.Errorf("Unexpected result")
	}
}

func TestDeckMergeImport(t *testing.T) {
	type Test struct {
		name         string
		new          *Deck
		existing     *Deck
		expected     bool
		expectedDeck *Deck
		err          string
	}
	tests := []Test{
		{
			name:     "different ids",
			new:      &Deck{ID: "deck-YWJjZAo"},
			existing: &Deck{ID: "deck-YWJjZQo"},
			err:      "IDs don't match",
		},
		{
			name:     "created timestamps don't match",
			new:      &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-15T00:00:00Z")},
			existing: &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-02-01T01:01:01Z"), Imported: parseTime("2017-01-20T00:00:00Z")},
			err:      "Created timestamps don't match",
		},
		{
			name:         "rounded timestamps",
			new:          &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-02-01T01:01:01.123Z"), Imported: parseTime("2017-01-20T00:00:00.123Z"), Modified: parseTime("2017-01-20T00:00:00.123Z")},
			existing:     &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-02-01T01:01:01Z"), Imported: parseTime("2017-01-20T00:00:00Z")},
			expected:     true,
			expectedDeck: &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-02-01T01:01:01.123Z"), Imported: parseTime("2017-01-20T00:00:00.123Z"), Modified: parseTime("2017-01-20T00:00:00.123Z")},
		},
		{
			name:     "new not an import",
			new:      &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-01-01T01:01:01Z")},
			existing: &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-15T00:00:00Z")},
			err:      "not an import",
		},
		{
			name:     "existing not an import",
			new:      &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-15T00:00:00Z")},
			existing: &Deck{ID: "deck-YWJjZAo", Created: parseTime("2017-01-01T01:01:01Z")},
			err:      "not an import",
		},
		{
			name: "new is newer",
			new: &Deck{
				ID:          "deck-YWJjZAo",
				Name:        "foo",
				Description: "FOO",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-15T00:00:00Z"),
				Cards:       CardCollection{"card-mjxwe-Zm9v.bmlsCg.0": {}},
			},
			existing: &Deck{
				ID:          "deck-YWJjZAo",
				Name:        "bar",
				Description: "BAR",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-01-01T01:01:01Z"),
				Imported:    parseTime("2017-01-20T00:00:00Z"),
				Cards:       CardCollection{"card-mjxwe-YmFy.bmlsCg.0": {}},
			},
			expected: true,
			expectedDeck: &Deck{
				ID:          "deck-YWJjZAo",
				Name:        "foo",
				Description: "FOO",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-15T00:00:00Z"),
				Cards:       CardCollection{"card-mjxwe-Zm9v.bmlsCg.0": {}},
			},
		},
		{
			name: "existing is newer",
			new: &Deck{
				ID:          "deck-YWJjZAo",
				Name:        "foo",
				Description: "FOO",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-01-01T01:01:01Z"),
				Imported:    parseTime("2017-01-15T00:00:00Z"),
				Cards:       CardCollection{"card-mjxwe-Zm9v.bmlsCg.0": {}},
			},
			existing: &Deck{
				ID:          "deck-YWJjZAo",
				Name:        "bar",
				Description: "BAR",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-20T00:00:00Z"),
				Cards:       CardCollection{"card-mjxwe-YmFy.bmlsCg.0": {}},
			},
			expected: false,
			expectedDeck: &Deck{
				ID:          "deck-YWJjZAo",
				Name:        "bar",
				Description: "BAR",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-20T00:00:00Z"),
				Cards:       CardCollection{"card-mjxwe-YmFy.bmlsCg.0": {}},
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := test.new.MergeImport(test.existing)
			testy.Error(t, test.err, err)
			if test.expected != result {
				t.Errorf("Unexpected result: %t", result)
			}
			if d := testy.DiffInterface(test.expectedDeck, test.new); d != nil {
				t.Error(d)
			}
		})
	}
}
