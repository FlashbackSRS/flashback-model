package test

import (
	"encoding/json"
	"testing"

	"gitlab.com/flimzy/testy"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

var frozenBundle = []byte(`
{
	"_id": "bundle-tui5ajfbabaeljnxt4om7fwmt4-krsxg5baij2w4zdmmu",
	"type": "bundle",
	"created": "2016-07-31T15:08:24.730156517Z",
	"modified": "2016-07-31T15:08:24.730156517Z",
	"imported": "2016-08-02T15:08:24.730156517Z",
	"name": "Test Bundle",
	"description": "A bundle for testing"
}
`)

func TestNewBundle(t *testing.T) {
	b, err := fb.NewBundle("krsxg5baij2w4zdmmu", "tui5ajfbabaeljnxt4om7fwmt4")
	testy.Error(t, "", err)

	b.Name = "Test Bundle"
	b.Created = now
	b.Modified = now
	b.Imported = now.AddDate(0, 0, 2)
	b.Description = "A bundle for testing"
	if want := "bundle-tui5ajfbabaeljnxt4om7fwmt4-krsxg5baij2w4zdmmu"; want != b.ID {
		t.Errorf("Unexpected bundle ID: %s", b.ID)
	}
	if d := testy.DiffAsJSON(testy.Snapshot(t), b); d != nil {
		t.Errorf("New Bundle:\n%s", d)
	}

	b2 := &fb.Bundle{}
	err = json.Unmarshal(frozenBundle, b2)
	testy.Error(t, "", err)
	if d := testy.DiffAsJSON(testy.Snapshot(t), b); d != nil {
		t.Errorf("Thawed Bundle:\n%s", d)
	}

	if d := testy.DiffInterface(b, b2); d != nil {
		t.Errorf("Thawed vs Created bundle:\n%s", d)
	}
}
