package test

import (
	"encoding/json"
	"strings"
	"testing"

	fb "gitlab.com/FlashbackSRS/flashback-model"
	"gitlab.com/flimzy/testy"
)

var frozenCard = []byte(`
{
	"_id": "card-tui5ajfbabaeljnxt4om7fwmt4-krsxg5baij2w4zdmmu.mViuXQThMLoh1G1Nlc4d_E8kR8o.0",
	"type": "card",
	"created": "2016-07-31T15:08:24.730156517Z",
	"modified": "2016-07-31T15:08:24.730156517Z",
	"imported": "2016-08-02T15:08:24.730156517Z",
	"model": "theme-VGVzdCBUaGVtZQ/0",
	"due": "2017-01-01",
	"interval": 50
}
`)

func TestCard(t *testing.T) {
	b, _ := fb.NewBundle("krsxg5baij2w4zdmmu", "tui5ajfbabaeljnxt4om7fwmt4")
	cardID := "card-" + strings.TrimPrefix(b.ID, "bundle-") + ".mViuXQThMLoh1G1Nlc4d_E8kR8o.0"
	c := fb.NewCard("theme-VGVzdCBUaGVtZQ", 0, cardID)

	c.Created = now
	c.Modified = now
	c.Imported = now.AddDate(0, 0, 2)
	due, _ := fb.ParseDue("2017-01-01")
	c.Due = due
	ivl, _ := fb.ParseInterval("50d")
	c.Interval = ivl
	if d := testy.DiffAsJSON(frozenCard, c); d != nil {
		t.Errorf("Created Card:\n%s", d)
	}

	c2 := &fb.Card{}
	err := json.Unmarshal(frozenCard, c2)
	testy.Error(t, "", err)
	if d := testy.DiffAsJSON(frozenCard, c2); d != nil {
		t.Errorf("Thawed card2:\n%s", d)
	}

	if d := testy.DiffInterface(c, c2); d != nil {
		t.Errorf("Thawed vs Created Cards:\n%s", d)
	}
}
