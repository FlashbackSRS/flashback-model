package v2

import (
	"encoding/json"
	"errors"
	"strings"
	"time"
)

type bundle struct {
	ID          string    `json:"_id"`
	Rev         string    `json:"_rev,omitempty"`
	Created     time.Time `json:"created"`
	Modified    time.Time `json:"modified"`
	Imported    time.Time `json:"imported,omitempty"`
	Name        string    `json:"name,omitempty"`
	Description string    `json:"description,omitempty"`
	Owner       string    `json:"owner"`
}

func (b *bundle) UnmarshalJSON(p []byte) error {
	type bundleAlias bundle
	ba := new(bundleAlias)
	if err := json.Unmarshal(p, &ba); err != nil {
		return err
	}
	*b = bundle(*ba)
	if b.ID != "" {
		parts := strings.Split(b.ID, "-")
		b.ID = "bundle-" + b.Owner + "-" + parts[len(parts)-1]
	}
	return b.readValidate()
}

// readValidate does a less-vigerous validation, for Unmarshaling.
func (b *bundle) readValidate() error {
	if b.ID == "" {
		return errors.New("id required")
	}
	if !strings.HasPrefix(b.ID, "bundle-") {
		return errors.New("incorrect doc type")
	}
	return nil
}
