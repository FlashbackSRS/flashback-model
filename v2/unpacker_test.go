package v2

import (
	"io"
	"strings"
	"testing"
	"time"

	"gitlab.com/flimzy/testy"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

var now = func() time.Time {
	t, _ := time.Parse(time.RFC3339, "2017-01-01T00:00:00Z")
	return t
}()

func TestUnpack(t *testing.T) {
	type tst struct {
		input    io.Reader
		expected *fb.Package
		err      string
	}
	tests := testy.NewTable()
	tests.Add("invalid json", tst{
		input: strings.NewReader("invalid json"),
		err:   "invalid character 'i' looking for beginning of value",
	})
	tests.Add("invalid bundle", tst{
		input: strings.NewReader(`{"version":1, "bundle":{}}`),
		err:   "id required",
	})
	tests.Add("invalid card", tst{
		input: strings.NewReader(`{"version":1, "bundle":{}, "cards":[{"type":"card"}]}`),
		err:   "id required",
	})
	tests.Add("invalid review", tst{
		input: strings.NewReader(`{"version":1, "reviews":[{}]}`),
		err:   "card id required",
	})
	tests.Add("no bundle", tst{
		input: strings.NewReader(`{"version":1,"cards":[]}`),
		err:   "bundle is nil",
	})
	tests.Add("full package", func(t *testing.T) interface{} {
		theme, _ := fb.NewTheme("theme-abcd")
		model := &fb.Model{
			Theme: theme,
			Type:  "foo",
			Files: theme.Attachments.NewView(),
		}
		theme.ModelSequence = 1
		theme.Models = []*fb.Model{model}
		theme.Created = now
		theme.Modified = now

		noteAtt := fb.NewFileCollection()

		return tst{
			input: strings.NewReader(`{
                "version": 1,
                "created": "2017-01-01T00:00:00Z",
                "modified": "2017-01-01T00:00:00Z",
                "bundle": {"_id":"bundle-mzxw6", "type":"bundle", "owner":"mjxwe", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z"},
                "themes": [{"_id":"theme-abcd", "type":"theme", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "modelSequence":1, "files":[], "_attachments":{}, "models":[{"fields":null, "files":[], "modelType":"foo", "templates":null, "id":0}]}],
                "decks": [{"_id":"deck-ZGVjaw", "type":"deck", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "cards":["card-mzxw6.bmlsCg.0"]}],
                "notes": [{"_id":"note-Zm9v", "type":"note", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "_attachments":{}, "fieldValues":null, "theme":"theme-abcd", "model":0}],
                "cards": [{"_id":"card-mzxw6.bmlsCg.0", "type":"card", "created":"2017-01-01T00:00:00Z", "modified":"2017-01-01T00:00:00Z", "model": "theme-abcd/0"}]
            }`),
			expected: &fb.Package{
				Created:  now,
				Modified: now,
				Bundle:   &fb.Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: now, Modified: now},
				Themes:   []*fb.Theme{theme},
				Decks:    []*fb.Deck{{ID: "deck-ZGVjaw", Created: now, Modified: now, Cards: fb.CardCollection{"card-mjxwe-mzxw6.bmlsCg.0": {}}}},
				Notes:    []*fb.Note{{ID: "note-Zm9v", ThemeID: "theme-abcd", Model: model, Created: now, Modified: now, Attachments: noteAtt}},
				Cards:    []*fb.Card{{ID: "card-mjxwe-mzxw6.bmlsCg.0", ModelID: "theme-abcd/0", Created: now, Modified: now}},
			},
		}
	})

	u := &Unpacker{}
	tests.Run(t, func(t *testing.T, test tst) {
		result, err := u.Unpack(test.input)
		testy.Error(t, test.err, err)
		if d := testy.DiffInterface(test.expected, result); d != nil {
			t.Error(d)
		}
	})
}
