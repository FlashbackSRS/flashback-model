package fb

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestCardCtxFieldValue(t *testing.T) {
	type tt struct {
		c        *CardCtx
		key      string
		expected *FieldValue
	}
	tests := testy.NewTable()
	tests.Add("not found", tt{
		c: &CardCtx{
			Model: &Model{},
		},
		key:      "Foo",
		expected: nil,
	})
	tests.Add("found", tt{
		c: &CardCtx{
			Model: &Model{
				Fields: []*Field{
					{Name: "Foo"},
					{Name: "Bar"},
				},
			},
			Note: &Note{
				FieldValues: []*FieldValue{
					{Text: "one"},
					{Text: "two"},
				},
			},
		},
		key:      "Bar",
		expected: &FieldValue{Text: "two"},
	})
	tests.Add("early exit", tt{
		c: &CardCtx{
			Model: &Model{
				Fields: []*Field{
					{Name: "Foo"},
					{Name: "Bar"},
				},
			},
			Note: &Note{
				FieldValues: []*FieldValue{
					{Text: "one"},
				},
			},
		},
		key:      "Bar",
		expected: nil,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := tt.c.FieldValue(tt.key)
		if d := testy.DiffInterface(tt.expected, result); d != nil {
			t.Error(d)
		}
	})
}
