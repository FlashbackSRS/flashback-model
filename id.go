package fb

import (
	"fmt"
)

const (
	docTypeUser  = "user"
	docTypeTheme = "theme"
	docTypeNote  = "note"
	docTypeDeck  = "deck"
	docTypeCard  = "card"
)

// EncodeDocID generates a DocID by encoding the docType and Base64-encoding
// the ID. No validation is done of the docType.
func EncodeDocID(docType string, id []byte) string {
	return fmt.Sprintf("%s-%s", docType, b64encoder.EncodeToString(id))
}
