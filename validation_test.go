package fb

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

type validator interface {
	Validate() error
}

type validationTest struct {
	name string
	v    validator
	err  string
}

func testValidation(t *testing.T, tests []validationTest) {
	t.Helper()
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Helper()
			err := test.v.Validate()
			testy.Error(t, test.err, err)
		})
	}
}
