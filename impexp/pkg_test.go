package impexp

import (
	"time"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

func completePackage() *fb.Package {
	t, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
	theme, _ := fb.NewTheme("theme-abcd")
	model := &fb.Model{
		Theme: theme,
		Type:  "foo",
		Files: theme.Attachments.NewView(),
	}
	theme.ModelSequence = 1
	theme.Models = []*fb.Model{model}
	theme.Created = t
	theme.Modified = t

	noteAtt := fb.NewFileCollection()

	return &fb.Package{
		Created:  t,
		Modified: t,
		Bundle:   &fb.Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: t, Modified: t},
		Themes:   []*fb.Theme{theme},
		Decks:    []*fb.Deck{{ID: "deck-ZGVjaw", Created: t, Modified: t, Cards: fb.CardCollection{"card-mjxwe-mzxw6.bmlsCg.0": {}}}},
		Notes:    []*fb.Note{{ID: "note-Zm9v", ThemeID: "theme-abcd", Model: model, Created: t, Modified: t, Attachments: noteAtt}},
		Cards:    []*fb.Card{{ID: "card-mjxwe-mzxw6.bmlsCg.0", ModelID: "theme-abcd/0", Created: t, Modified: t}},
	}
}
