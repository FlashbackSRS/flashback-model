package impexp

import (
	"bytes"
	"errors"
	"io"
	"os"
	"strings"
	"testing"

	"gitlab.com/flimzy/testy"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

func TestUnpackJSON(t *testing.T) {
	type tst struct {
		input    io.Reader
		expected *fb.Package
		err      string
	}
	tests := testy.NewTable()
	tests.Add("invalid json", tst{
		input: strings.NewReader("invalid json"),
		err:   "invalid character 'i' looking for beginning of value",
	})
	tests.Add("read error", tst{
		input: testy.ErrorReader("", errors.New("read error")),
		err:   "read error",
	})
	tests.Add("unrecognized version", tst{
		input: strings.NewReader(`{"version":"invalid"}`),
		err:   "package version `invalid` not supported",
	})
	tests.Add("v2 long buffer", tst{
		// Lots of padding, to ensure the entire object doesn't fit within
		// the first 256 bytes.
		input: strings.NewReader(`                                             {
                                                                       "version"
                                                                               :
                                                                               2
                                                                               }
                                                                               `),
		err: "bundle is nil",
	})
	tests.Add("v2 short buffer", tst{
		input: strings.NewReader(`{"version":2}`),
		err:   "bundle is nil",
	})
	tests.Add("v3 long buffer", tst{
		input: strings.NewReader(`{"version":3}
		{"bundle": {"_id": "bundle-mjxwe-aebagbb","type": "bundle","created": "2016-07-31T15:08:24.730156517Z",
		"modified": "2016-07-31T15:08:24.730156517Z","owner": "mjxwe"},"cards": [{"type": "card",
		"_id": "card-mjxwe-aebagbb.VGVzdCBOb3Rl.0","created": "2016-07-31T15:08:24.730156517Z",
		"modified": "2016-07-31T15:08:24.730156517Z","model": "theme-VGVzdCBUaGVtZQ/0"
		}],"notes": [{"_id": "note-VGVzdCBOb3Rl","type": "note","created": "2016-07-31T15:08:24.730156517Z",
		"modified": "2016-07-31T15:08:24.730156517Z","imported": "2016-08-02T15:08:24.730156517Z",
		"theme": "theme-VGVzdCBUaGVtZQ","model": 0,"fieldValues": [{"text": "cat"}]}],
		"decks": [{"_id": "deck-VGVzdCBEZWNr","type": "deck","created": "2016-07-31T15:08:24.730156517Z",
		"modified": "2016-07-31T15:08:24.730156517Z","imported": "2016-08-02T15:08:24.730156517Z",
		"name": "Test Deck","description": "Deck for testing","cards": ["card-mjxwe-aebagbb.VGVzdCBOb3Rl.0"]
		}],"themes": [{"_id": "theme-VGVzdCBUaGVtZQ","type": "theme","created": "2016-07-31T15:08:24.730156517Z",
		"modified": "2016-07-31T15:08:24.730156517Z","imported": "2016-08-02T15:08:24.730156517Z",
		"name": "Test Theme","description": "Theme for testing","models": [{"id": 0,"modelType": "anki-basic",
		"name": "Model A","templates": [],"fields": [{"fieldType": 0,"name": "Word"}],"files": [
		"m1.html"]}],"_attachments": {"$main.css": {"content_type": "text/css","data": "LyogYW4gZW1wdHkgQ1NTIGZpbGUgKi8="
		},"m1.html": {"content_type": "text/html","data": "PGh0bWw+PC9odG1sPg=="}},"files": [
		"$main.css"],"modelSequence": 2}],"reviews": [{"cardID": "card-krsxg5baij2w4zdmmu.VGVzdCBOb3Rl.0",
		"timestamp": "2017-01-01T01:01:01Z"}]`),
		err: "unexpected EOF",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		result, err := unpackJSON(test.input)
		testy.Error(t, test.err, err)
		if d := testy.DiffInterface(test.expected, result); d != nil {
			t.Error(d)
		}
	})
}

func TestPackUnpack(t *testing.T) {
	pkg := completePackage()
	pack := &bytes.Buffer{}
	if err := Pack(pack, pkg); err != nil {
		t.Fatal(err)
	}
	save(t, "testdata/v3.fbb", pack.Bytes())
	result, err := Unpack(pack)
	if err != nil {
		t.Fatal(err)
	}
	if d := testy.DiffInterface(pkg, result); d != nil {
		t.Error(d)
	}
}

func TestUnpackVersions(t *testing.T) {
	expected := completePackage()
	versions := []string{"v2", "v3"}
	for _, ver := range versions {
		t.Run(ver, func(t *testing.T) {
			f, err := os.Open("testdata/" + ver + ".fbb")
			if err != nil {
				t.Fatal(err)
			}
			result, err := Unpack(f)
			if err != nil {
				t.Fatal(err)
			}
			if d := testy.DiffInterface(expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}
