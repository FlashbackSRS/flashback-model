// +build update

package impexp

import (
	"io/ioutil"
	"testing"
)

func save(t *testing.T, filename string, content []byte) {
	if err := ioutil.WriteFile(filename, content, 0644); err != nil {
		t.Fatal(err)
	}
}
