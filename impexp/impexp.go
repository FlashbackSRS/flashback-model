// Package impexp provides import and export capabilities for the flashback model.
package impexp

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"io"
	"io/ioutil"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

// Unpack reads a FBB file from r, and returns a Package.
func Unpack(r io.Reader) (*fb.Package, error) {
	gunzip, err := gzip.NewReader(r)
	if err != nil {
		return nil, err
	}
	defer gunzip.Close() // nolint: errcheck
	return unpackJSON(gunzip)
}

// This is plenty to read just the header
const maxHeaderSize = 256

// readHeader reads the header from in, and returns the version, and the
// remainder of the unused JSON buffer.
func readHeader(r io.Reader) (string, io.Reader, error) {
	header, err := ioutil.ReadAll(io.LimitReader(r, maxHeaderSize))
	if err != nil {
		return "", nil, err
	}
	h := new(struct {
		Version json.RawMessage `json:"version"`
	})
	hr := bytes.NewReader(header)
	dec := json.NewDecoder(hr)
	if err := dec.Decode(&h); err != nil {
		if err.Error() == "unexpected EOF" {
			// This means no header, so must be version 2
			h.Version = []byte("2")
			err = nil
		} else {
			return "", nil, err
		}
	}
	version := string(bytes.Trim(h.Version, `"`))
	switch version {
	case "1", "2":
		// Replay the entire buffer for old versions
		r = io.MultiReader(bytes.NewReader(header), r)
	default:
		// Return only the unused portion of the buffer for new versions
		r = io.MultiReader(dec.Buffered(), hr, r)
	}
	return version, r, nil
}

// unpackJSON unpacks uncompressed JSON
func unpackJSON(r io.Reader) (*fb.Package, error) {
	version, r, err := readHeader(r)
	if err != nil {
		return nil, err
	}

	unpacker, err := unpacker(version)
	if err != nil {
		return nil, err
	}
	return unpacker.Unpack(r)
}

// Pack packages pkg for file storage or network transmission.
func Pack(out io.Writer, pkg *fb.Package) error {
	if err := writeHeader(out); err != nil {
		return err
	}
	w := gzip.NewWriter(out)
	if err := json.NewEncoder(w).Encode(pkg); err != nil {
		return err
	}
	return w.Close()
}

const (
	formatVersion = 3
)

func writeHeader(out io.Writer) error {
	type header struct {
		Version int `json:"version"`
	}

	w := gzip.NewWriter(out)
	if err := json.NewEncoder(w).Encode(&header{Version: formatVersion}); err != nil {
		return err
	}
	return w.Close()
}
