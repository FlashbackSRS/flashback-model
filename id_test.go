package fb

import (
	"testing"
)

func TestEncodeDocID(t *testing.T) {
	expected := "foo-dGVzdCBpZA"
	result := EncodeDocID("foo", []byte("test id"))
	if result != expected {
		t.Errorf("Unexpected result: %s", result)
	}
}
