package fb

import "time"

// CardCtx embeds a card, and contains all other context necessary to render a
// specific card.
type CardCtx struct {
	*Card
	Face  int
	Model *Model
	Note  *Note
	// Done indicates whether the studying of this card is done, and the next
	// card should be selected. It is set by the Action() method.
	Done bool
	// UnblockNext indicates whether to unblock the next related card. Typically
	// this is done when the studied card reaches maturity, or in the case of
	// an introductory card, it could be immediately.
	UnblockNext bool
	// NextCard indicates the ID of the next card to be displayed. It can be
	// used, for example, by an intro card, to indicate that the related
	// study card should immediately follow. In the future, it may also be
	// used to link related cards that should be studied in a strict order.
	NextCard string
	// StartTime is the time the question was displayed, used to calculate
	// the answer delay.
	StartTime time.Time
	// Query is the query payload from the card.
	Query interface{}
}

// FieldValue returns the field value associated with the requested key.
func (c *CardCtx) FieldValue(key string) *FieldValue {
	for i, field := range c.Model.Fields {
		if len(c.Note.FieldValues) < i+1 {
			return nil
		}
		if key == field.Name {
			return c.Note.FieldValues[i]
		}
	}
	return nil
}
