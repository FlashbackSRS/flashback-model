module gitlab.com/FlashbackSRS/flashback-model

go 1.14

require (
	github.com/go-kivik/kivik/v3 v3.0.2
	github.com/otiai10/copy v1.1.1 // indirect
	github.com/pkg/errors v0.9.1
	gitlab.com/flimzy/testy v0.1.1
)
