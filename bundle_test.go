package fb

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestNewBundle(t *testing.T) {
	tests := []struct {
		name     string
		id       string
		owner    string
		expected *Bundle
		err      string
	}{
		{
			name:  "no id",
			owner: "mjxwe",
			err:   "id required",
		},
		{
			name:  "valid",
			id:    "mzxw6",
			owner: "mjxwe",
			expected: &Bundle{
				ID:       "bundle-mjxwe-mzxw6",
				Owner:    "mjxwe",
				Created:  shortNow,
				Modified: shortNow,
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := NewBundle(test.id, test.owner)
			testy.Error(t, test.err, err)
			if d := testy.DiffInterface(test.expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestBundleMarshalJSON(t *testing.T) {
	tests := []struct {
		name     string
		bundle   *Bundle
		expected string
		err      string
	}{
		{
			name: "null fields",
			bundle: &Bundle{
				ID:       "bundle-mjxwe-mzxw6",
				Owner:    "mjxwe",
				Created:  now(),
				Modified: now(),
			},
			expected: `{
				"_id":      "bundle-mjxwe-mzxw6",
				"type":     "bundle",
				"created":  "2017-01-01T00:00:00Z",
				"modified": "2017-01-01T00:00:00Z"
			}`,
		},
		{
			name: "all fields",
			bundle: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Created:     now(),
				Modified:    now(),
				Imported:    now(),
				Name:        "foo name",
				Description: "foo description",
			},
			expected: `{
				"_id":         "bundle-mjxwe-mzxw6",
				"type":        "bundle",
				"name":        "foo name",
				"description": "foo description",
				"created":     "2017-01-01T00:00:00Z",
				"modified":    "2017-01-01T00:00:00Z",
				"imported":    "2017-01-01T00:00:00Z"
			}`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := test.bundle.MarshalJSON()
			testy.Error(t, test.err, err)
			if d := testy.DiffJSON([]byte(test.expected), result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestBundleUnmarshalJSON(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected *Bundle
		err      string
	}{
		{
			name:  "invalid json",
			input: "invalid json",
			err:   "failed to unmarshal Bundle: invalid character 'i' looking for beginning of value",
		},
		{
			name: "null fields",
			input: `{
				"_id":      "bundle-mjxwe-mzxw6",
				"owner":    "mjxwe",
				"created":  "2017-01-01T00:00:00Z",
				"modified": "2017-01-01T00:00:00Z"
			}`,
			expected: &Bundle{
				ID:       "bundle-mjxwe-mzxw6",
				Owner:    "mjxwe",
				Created:  shortNow,
				Modified: shortNow,
			},
		},
		{
			name: "all fields",
			input: `{
                "_id":         "bundle-mjxwe-mzxw6",
                "name":        "foo name",
                "description": "foo description",
                "created":     "2017-01-01T00:00:00Z",
                "modified":    "2017-01-01T00:00:00Z",
                "imported":    "2017-01-01T00:00:00Z"
            }`,
			expected: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Created:     shortNow,
				Modified:    shortNow,
				Imported:    shortNow,
				Name:        "foo name",
				Description: "foo description",
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := &Bundle{}
			err := result.UnmarshalJSON([]byte(test.input))
			testy.Error(t, test.err, err)
			if d := testy.DiffInterface(test.expected, result); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestBundleSetRev(t *testing.T) {
	bundle := &Bundle{}
	rev := rev1
	bundle.SetRev(rev)
	if bundle.Rev != rev {
		t.Errorf("failed to set rev")
	}
}

func TestBundleID(t *testing.T) {
	expected := "bundle-mjxwe-mzxw6"
	bundle := &Bundle{ID: "bundle-mjxwe-mzxw6"}
	if id := bundle.DocID(); id != expected {
		t.Errorf("unexpected id: %s", id)
	}
}

func TestBundleImportedTime(t *testing.T) {
	t.Run("Set", func(t *testing.T) {
		bundle := &Bundle{}
		ts := now()
		bundle.Imported = ts
		if it := bundle.ImportedTime(); it != ts {
			t.Errorf("Unexpected result: %s", it)
		}
	})
	t.Run("Unset", func(t *testing.T) {
		bundle := &Bundle{}
		if it := bundle.ImportedTime(); !it.IsZero() {
			t.Errorf("unexpected result: %v", it)
		}
	})
}

func TestBundleModifiedTime(t *testing.T) {
	bundle := &Bundle{}
	ts := now()
	bundle.Modified = ts
	if mt := bundle.ModifiedTime(); mt != ts {
		t.Errorf("Unexpected result")
	}
}

func TestBundleMergeImport(t *testing.T) {
	type Test struct {
		name           string
		new            *Bundle
		existing       *Bundle
		expected       bool
		expectedBundle *Bundle
		err            string
	}
	tests := []Test{
		{
			name:     "different ids",
			new:      &Bundle{ID: "bundle-mjxwe-mzxw6"},
			existing: &Bundle{ID: "bundle-mjxwe-mjqxecq"},
			err:      "IDs don't match",
		},
		{
			name:     "created timestamps don't match",
			new:      &Bundle{ID: "bundle-mjxwe-mzxw6", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-15T00:00:00Z")},
			existing: &Bundle{ID: "bundle-mjxwe-mzxw6", Created: parseTime("2017-02-01T01:01:01Z"), Imported: parseTime("2017-01-20T00:00:00Z")},
			err:      "Created timestamps don't match",
		},
		{
			name:           "rounding timestamps",
			new:            &Bundle{ID: "bundle-mjxwe-mzxw6", Created: parseTime("2017-02-01T01:01:01.123Z"), Imported: parseTime("2017-01-20T00:00:00.123Z"), Modified: parseTime("2017-01-20T00:00:00.123Z")},
			existing:       &Bundle{ID: "bundle-mjxwe-mzxw6", Created: parseTime("2017-02-01T01:01:01Z"), Imported: parseTime("2017-01-20T00:00:00Z")},
			expected:       true,
			expectedBundle: &Bundle{ID: "bundle-mjxwe-mzxw6", Created: parseTime("2017-02-01T01:01:01.123Z"), Imported: parseTime("2017-01-20T00:00:00.123Z"), Modified: parseTime("2017-01-20T00:00:00.123Z")},
		},
		{
			name:     "owners don't match",
			new:      &Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-15T00:00:00Z")},
			existing: &Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mfwgsy3fbi", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-20T00:00:00Z")},
			err:      "Cannot change bundle ownership",
		},
		{
			name:     "new not an import",
			new:      &Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: parseTime("2017-01-01T01:01:01Z")},
			existing: &Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-15T00:00:00Z")},
			err:      "not an import",
		},
		{
			name:     "existing not an import",
			new:      &Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: parseTime("2017-01-01T01:01:01Z"), Imported: parseTime("2017-01-15T00:00:00Z")},
			existing: &Bundle{ID: "bundle-mjxwe-mzxw6", Owner: "mjxwe", Created: parseTime("2017-01-01T01:01:01Z")},
			err:      "not an import",
		},
		{
			name: "new is newer",
			new: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Name:        "foo",
				Description: "FOO",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-15T00:00:00Z"),
			},
			existing: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Name:        "bar",
				Description: "BAR",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-01-01T01:01:01Z"),
				Imported:    parseTime("2017-01-20T00:00:00Z"),
			},
			expected: true,
			expectedBundle: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Name:        "foo",
				Description: "FOO",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-15T00:00:00Z"),
			},
		},
		{
			name: "existing is newer",
			new: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Name:        "foo",
				Description: "FOO",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-01-01T01:01:01Z"),
				Imported:    parseTime("2017-01-15T00:00:00Z"),
			},
			existing: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Name:        "bar",
				Description: "BAR",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-20T00:00:00Z"),
			},
			expected: false,
			expectedBundle: &Bundle{
				ID:          "bundle-mjxwe-mzxw6",
				Owner:       "mjxwe",
				Name:        "bar",
				Description: "BAR",
				Created:     parseTime("2017-01-01T01:01:01Z"),
				Modified:    parseTime("2017-02-01T01:01:01Z"),
				Imported:    parseTime("2017-01-20T00:00:00Z"),
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := test.new.MergeImport(test.existing)
			testy.Error(t, test.err, err)
			if test.expected != result {
				t.Errorf("Unexpected result: %t", result)
			}
			if d := testy.DiffInterface(test.expectedBundle, test.new); d != nil {
				t.Error(d)
			}
		})
	}
}

func TestBundleDisplayName(t *testing.T) {
	type tst struct {
		bundle   *Bundle
		expected string
	}
	tests := testy.NewTable()
	tests.Add(" name", tst{
		bundle:   &Bundle{Name: "Test bundle"},
		expected: "Test bundle",
	})
	tests.Add("fallback to ID", tst{
		bundle:   &Bundle{ID: "bundle-foo-bar"},
		expected: "{foo-bar}",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		result := test.bundle.DisplayName()
		if result != test.expected {
			t.Errorf("Expected: %s\n  Actual: %s\n", test.expected, result)
		}
	})
}
